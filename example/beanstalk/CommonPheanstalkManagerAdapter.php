<?php

use Psr\Log\LoggerInterface;

class CommonPheanstalkManagerAdapter extends AbstractPheanstalkManagerAdapter
{
    /** @var  LoggerInterface */
    private $logger;

    /**
     * @param LoggerInterface $logger
     *
     * @return CommonPheanstalkManagerAdapter
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;

        return $this;
    }

    protected function log($message, array $context = array())
    {
        $this->logger->info($message, $context);
    }

    protected function logEmail($email, $subject, $body, $tag = '')
    {
        $this->logger->info(
            'Email data added to queue',
            array(
                'email'   => $email,
                'subject' => $subject,
                'body'    => $body,
                'tag'     => $tag,
            )
        );
    }
}