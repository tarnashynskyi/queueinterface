<?php

use LetyShops\MailSender\Adapters\Queue\QueueJobAdapterInterface;
use LetyShops\MailSender\Adapters\Queue\QueueManagerAdapterInterface;
use Pheanstalk\Exception\ServerException;
use Pheanstalk\Pheanstalk;
use Pheanstalk\PheanstalkInterface;

abstract class AbstractPheanstalkManagerAdapter implements QueueManagerAdapterInterface
{
    /** @var  Pheanstalk */
    protected $beanstalk;

    /** @var string Beanstalk tube name */
    protected $beanstalkTubeName;

    /**
     * AbstractBeanstalkManager constructor.
     *
     * @param null|string $beanstalkTubeName Beanstalk tube name
     * @param null|string $beanstalkHost     Beanstalk server host
     * @param null|int    $beanstalkPort     Beanstalk server port
     * @param             $reserveTimeout
     */
    public function __construct($beanstalkTubeName, $beanstalkHost = null, $beanstalkPort = null)
    {
        $this->beanstalkTubeName = $beanstalkTubeName;

        $this->beanstalk = new Pheanstalk($beanstalkHost, $beanstalkPort);
    }

    /**
     * @param     $data
     *
     * @param int $priority
     * @param int $delay
     * @param int $ttr
     *
     * @return int|\Pheanstalk\Response
     */
    public function put(
        $data,
        $priority = PheanstalkInterface::DEFAULT_PRIORITY,
        $delay = PheanstalkInterface::DEFAULT_DELAY,
        $ttr = PheanstalkInterface::DEFAULT_TTR
    )
    {
        return $this->beanstalk->putInTube($this->beanstalkTubeName, $data, $priority, $delay, $ttr);
    }

    /**
     * @param null $timeout
     *
     * @return bool|QueueJobAdapterInterface
     */
    public function reserve($timeout = null)
    {
        $job = $this->beanstalk->reserveFromTube($this->beanstalkTubeName, $timeout);

        if ($job) {
            return new PheanstalkJobAdapter($job);
        }

        return $job;
    }

    /**
     * @param object $job Job
     *
     * @return mixed
     */
    public function release(QueueJobAdapterInterface $job)
    {
        return $this->beanstalk->release($job);
    }

    /**
     * @param object $job Job
     *
     * @return mixed
     */
    public function delete(QueueJobAdapterInterface $job)
    {
        return $this->beanstalk->delete($job);
    }

    /**
     * @return string
     */
    public function getBeanstalkTubeName()
    {
        return $this->beanstalkTubeName;
    }

    /**
     * @return \Pheanstalk\Connection
     */
    public function getConnection()
    {
        return $this->beanstalk->getConnection();
    }

    /**
     * Статистика очереди
     *
     * @return object|array|\Pheanstalk\Response
     */
    public function stats()
    {
        $currentTubeStatus = array(
            'current-jobs-ready' => 0,
        );

        if (!in_array($this->beanstalkTubeName, $this->beanstalk->listTubes())) {
            return $currentTubeStatus;
        }

        try {
            $currentTubeStatus = $this->beanstalk->statsTube($this->beanstalkTubeName);
        } catch (ServerException $e) {
            $this->log(
                'Error on Beanstalk occurred',
                array(
                    'exception' => array(
                        'class'   => get_class($e),
                        'message' => $e->getMessage(),
                    ),
                )
            );
        }

        return $currentTubeStatus;
    }

    /**
     * Получение количества элементов в очереди
     *
     * @return int
     */
    public function getCurrentJobReady()
    {
        return (int)$this->stats()['current-jobs-ready'];
    }

    abstract protected function log($message, array $context = array());

    abstract protected function logEmail($email, $subject, $body, $tag = '');
}