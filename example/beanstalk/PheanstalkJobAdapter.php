<?php

use LetyShops\MailSender\Adapters\Queue\QueueJobAdapterInterface;
use Pheanstalk\Job;

class PheanstalkJobAdapter implements QueueJobAdapterInterface
{
    /** @var  Job */
    protected $job;

    /**
     * BeanstalkJobAdapter constructor.
     *
     * @param Job $job
     */
    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    /**
     * @{@inheritdoc}
     */
    public function getId()
    {
        return $this->job->getId();
    }

    /**
     * @{@inheritdoc}
     */
    public function getData()
    {
        return $this->job->getData();
    }

    public function getOriginalJob()
    {
        return $this->job;
    }
}