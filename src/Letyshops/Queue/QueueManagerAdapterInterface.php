<?php

namespace LetyShops\Queue;


interface QueueManagerAdapterInterface
{
    /**
     * Добавление задания с данными $data, приоритетом $priority, задержкой $delay и временем на выполнение $ttr
     * в очередь
     *
     * @param string $data     Данные задания
     * @param int    $priority Приоритет
     * @param int    $delay    Задержка
     * @param int    $ttr      Время выполнения задания
     *
     * @return int
     */
    public function put($data, $priority = 1204, $delay = 0, $ttr = 60);

    /**
     * Попытка резервирования нового задания
     *
     * @param int $timeout Таймаут резервирования задания.
     *                     По завершению таймаута возращается falsy значение и пробуем снова
     *
     * @return QueueJobAdapterInterface|null|false|int
     */
    public function reserve($timeout = null);

    /**
     * Возвращение задания в очередь
     *
     * @param QueueJobAdapterInterface $job Задание
     *
     * @return mixed
     */
    public function release(QueueJobAdapterInterface $job);

    /**
     * Удаление задания из очереди
     *
     * @param QueueJobAdapterInterface $job Задание
     *
     * @return mixed
     */
    public function delete(QueueJobAdapterInterface $job);

    /**
     * Получение количества заданий в очереди
     *
     * @return int
     */
    public function getCurrentJobReady();
}