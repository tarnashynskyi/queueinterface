<?php

namespace LetyShops\Queue;


interface QueueJobAdapterInterface
{
    /**
     * Возвращает id задания
     *
     * @return int
     */
    public function getId();

    /**
     * Возвращает данные задание
     *
     * @return string
     */
    public function getData();
}